
*{{ site.description }}*

This is the Genesis block for all BlockRing™ websites :
 [{{ site.url }}]({{site.url}})

![{{qm}}](https://placebeard.it/1200x600)

<small>*note: this site is deployed on [netlify][nl] on %date%</small>
<hr>
<small>
Copyleft <span class="copyleft">&copy;</span> {{ site.businame }} {{ site.time | date: '%Y' }}
@licence: <http://www.wtfpl.net> [WTFPL]({{site.search_url}}=WTFPL+license)
</small>

<!-- @license   http://www.gnu.org/copyleft/lesser.html LGPL-->

[nl]: https://app.netlify.com/sites/{{site.netname}}/deploys



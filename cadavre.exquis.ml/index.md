## %domain%


This site (%loc%) is the genesis of website deployed on the [blockRing™][brng]

The principle is simple with any URL a [canonical URI][CURI] is created
and is presented to the browser as the "mutable" pointer to the website files


rr: %rr%

our license is very permissive : [LICENCE](LICENCE.htm)

oh by the way check your <span id=ip>ip</span> ([%ip%](test/ip.htm))

[brng]: {{DUCK}}=!g+%22blockRing™%22
[CURI]: {{DUCK}}=!g+cadavre+exquis+mutable+%23CURI

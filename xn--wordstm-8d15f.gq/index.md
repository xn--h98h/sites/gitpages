
## Have a blue footprint !

All sites w/ a 🔷™ in their names are part of our trademark
for all [zero-waste][ZW] operations and products, and you can be part
of the movements too.

We believe every on should switch urgently to consume solely 
"[blue products][BlueP]".

If you too are producing according to the universal law and
not generating waste along... ask for your blue diamond label.

----

### This site config variables are :

* title: [%title%](http://www.%domain%/)
* tics: %tics%
* date: %date%

* hero: %hero%
* description: %description%
* email: [%email%](mailto:%email%)

* loc: <%loc%>
* hostname: [%hostname%](http://%hostname%/)
* domain: [%domain%](https://%domain%/)
* origin: <%origin%>
* url: <%url%>


[BlueP]: {{DUCK}}=blue+industry+zero+waste
[ZW]: https://greenblue.org/making-zero-net-waste-a-reality-for-the-plastics-industry/

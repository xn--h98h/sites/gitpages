---
title: Ladonna J. Revay
description: Why Ladonna J. Revay is empty handed ?
layout: page
---

# {{page.description}}



sha3-224(0x0a020801) = 17bc8b34c0bb0f32d7c0a03dd912b02380e6997dc27b2318a05169b6;\
w/ multihash header:\
  171c.17bc8b34c0bb0f32d7c0a03dd912b02380e6997dc27b2318a05169b6\
encoded in base 58:\
  [z6cYNbecZSFzLjbSimKuibtdpGt7DAUMMt46aKQNdwfs™][empty-dir-sha3-224]

funiq: bc8b34c0bb0f\
luniq: 32d7c0a0\

encoded in base [5494](http://ipfs.io/ipfs/QmVcbzHfkv2FB2agXeHUhkZdCe7ocHKeKmBF9ZyGcj64Md) : 1250,569,5383,1377 -> Ladonna, Jaime\
encoded in base [88799](http://ipfs.io/ipfs/QmZj8rRYZjaCZDv3ZFpqioEFCsaUuidbmpLd4MV6B9TXN4): 34618,81551 -> Revay, Casper

Therefore empty-dir => Ladonna J. Revay

```
my $funiq = substr($bin,1,6); # 6 char (except 1st)
my $luniq = substr($bin,7,4);  # 4 char 
my @first = map { $flist->[$_] } &encode_baser($funiq,5494);
my @last = map { $llist->[$_] } &encode_baser($luniq,88799);
```

-- \
bajub-disis-divus-mosuh-safur-busuf-tizab-pabut-tohif-rabog-magok-nojut-sanur-fasim-padid-kokuk



[empty-dir-sha3-224]: {{site.search}}=!g+z6cYNbecZSFzLjbSimKuibtdpGt7DAUMMt46aKQNdwfs

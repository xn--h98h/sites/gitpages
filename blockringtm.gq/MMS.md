---
layout: page
title: Who is Mathew M. Skeele ?
description: Why Mathew M. Skeele is empty handed ?
---
# Why Mathew M. Skeele is empty handed ?


sha3-224('') = c6b4e03423667dbb73b6e15454f0eb1abd4597f9a1b078e3f5b5a6bc7;\
w/ multihash header:\
  171c.c6b4e03423667dbb73b6e15454f0eb1abd4597f9a1b078e3f5b5a6bc7\
encoded in base 58:\
  [z6CfPsNrajGLLoNHWshz5fm6JwY2HBYLAyTARUUwwhWe™][emptyf-sha3-224]

funiq: 4e03423667db\
luniq: b73b6e15\

encoded in base [5494](http://ipfs.io/ipfs/QmVcbzHfkv2FB2agXeHUhkZdCe7ocHKeKmBF9ZyGcj64Md) : 517,1365,4738,4163 -> Mathew, Marylou\
encoded in base [88799](http://ipfs.io/ipfs/QmZj8rRYZjaCZDv3ZFpqioEFCsaUuidbmpLd4MV6B9TXN4): 34618,81551 -> Skeele, Rothstein

Therefore '' => Mathew M. Skeele 

```
my $funiq = substr($bin,1,6); # 6 char (except 1st)
my $luniq = substr($bin,7,4);  # 4 char 
my @first = map { $flist->[$_] } &encode_baser($funiq,5494);
my @last = map { $llist->[$_] } &encode_baser($luniq,88799);
```

-- \
bajij-disis-kotav-bataf-ginol-tovul-gotov-dijaj-husav-rakor-tidin-luvip-dosal-mumuz-jotip-kozal


[emptyf-sha3-224]: {{site.search}}=!g+z6CfPsNrajGLLoNHWshz5fm6JwY2HBYLAyTARUUwwhWe

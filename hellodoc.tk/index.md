# %title%

%description%

*HelloDoc* is your online virtual health assistant to help you stay healthy.

contact: [%email%](mailto:%email%)

[brng]: {{DUCK}}=!g+%22blockRing™%22
[HIP]: {{DUCK}}=!g+%23HIP6+human+IP+address
[HA]: {{DUCK}}=!g+%23Healthium+Application

---
[![%tag%](https://data.jsdelivr.com/v1/package/gh/iglake/js/badge?style=rounded)](https://www.jsdelivr.com/package/gh/iglake/js)

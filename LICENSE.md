---
---
---
##          DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
####                 Version n, August 2019
####                       WTFPL[^1]

 Copyleft © 2019 Michel Combes <michel@gc-bank.org>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document[^2], and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
  1. And I am doing it too.
 
[^1]: WTFPL ~ [WHAT THE FUCK PUBLIC LICENCE](http://www.wtfpl.net/about/)&nbsp;
[^2]: [plain text](LICENCE.txt) licence&nbsp; 

<style> #do-what-the-fuck-you-want-to-public-license, #version-n-august-2019, #wtfpl1 { text-align: center } </style>

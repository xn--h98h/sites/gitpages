# %title%

*%description%*

This site is for exploring your [blockRing™][1] [space][/],
as well as your [friends][2]' space.


- [/my/*][/my]
- [/our/*][/our]
- [/public/*][/public]

- [/root/*][/root]

You have to have a [IPFS][3] node running locally ...


[1]: {{DUCK}}=%22blockRing™%22
[2]: %files%/my/friends/
[3]: {{DUCK}}=!g+running+an+IPFS+node+locally

[/my]: %files%/my
[/our]: %files%/our
[/public]: %files%/public
[/root]: %files%/root
[/]: %files%


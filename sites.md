## websites 


- a website in [30 seconds](30sec)

- [Electronic Records](https://ER℠.gq)
- [Electronic Health Record](https://EHR™.ml)
- [EmergeIT.tk](https://emergeIT.tk) &amp; [EmergeIT.ml](https://emergeIT.ml)

- [blockRing™](https://www.blockRing™.gq)
- [explorer™.ml](https://explorer™.ml)

- [Cadavre Exquis](https://cadavre.exquis.ml)
- locally: <http://127.0.0.1>

- Healthium™: <https://healthium.gq>
- HelloDoc: ([tk](https://hellodoc.tk) &amp; [gq](https://hellodoc.gq))
- <https://hostless.ml>
- <https://iuris.gq>
- <https://matters.tk>
- <https://myfocus.ml>
- <https://record™.ml>
- <https://resume℠.gq>
- <https://xn--wordstm-8d15f.gq>

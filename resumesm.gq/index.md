---
layout: home
site: Resume Service Mark
background-img: /ipfs/Z5wZ9oUSpsyhALkyTpRDomMmrpAhXjB9QV8MdBHXhSNViExbyqjedvYsJ5jkJ6LUiL4ccx/focus.jpg
title: Resume Service Mark
description: a resume on the blockchain for everyone
---

our official site is <{{site.url}}>

----
{% include README.md %}
----

more <a href="{{site.search_url}}=!g+inurl:%22...%22">...</a>

